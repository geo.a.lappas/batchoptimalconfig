from tensorflow.python.client import device_lib
from tensorflow import keras
from tensorflow.python.keras.engine.functional import Functional
from tabulate import tabulate
from keras.utils.layer_utils import count_params
from keras.applications.efficientnet_v2 import EfficientNetV2B3 as EfficientNet
from keras.engine.input_layer import InputLayer as InputLayer

import numpy as np
import pandas as pd
import model_profiler
import tensorflow as tf
import keras.models
import os
import warnings

def dynamcGpuAlloc():
    availableDevices = tf.config.list_physical_devices('GPU')
    list_config_temp = [itr for itr in range(len(availableDevices))]
    list_config = ','.join(str(e) for e in list_config_temp)
    os.environ["CUDA_VISIBLE_DEVICES"] = list_config
    os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
    warnings.filterwarnings("ignore")

    print(len(availableDevices), 'GPUs found.')

    return len(availableDevices)

def lstToDict(list1: list, list2: list):
    '''
    Concatenate two lists in one dictionary for easy mapping.

    :param list1: list #1
    :param list2: list #2
    :return: dictionary composed of two lists
    '''
    # Concatenate two lists in one dictionary

    if not(isinstance(list1, list)) or not(isinstance(list2, list)):
        raise AssertionError('One of both input arguments are not lists.')

    if len(list1) != len(list2):
        raise AssertionError('The lists should have the same shape.')

    convDict = {list1[itrConv]: list2[itrConv] for itrConv in range(0, len(list1))}

    return convDict

def modelProfiler(model:keras.models, batchSize: int, volumeSize: tuple, channelIn: int, channelOut: int, gpuDevice: list):
    '''
    This function will calculate the required memory and information related to the given setup.
    NOTE: currently works only for 2D float32, single channel
    Parameters
    model : tensorflow model. Supports both sequential or functional
    batchSize : batch size
    volumeSize : tuple with the volume size
    channelIn : number of input channels
    channelOut : number of output channels

    Returns
    profile: object with memory requirements and information related to the current setup
    '''

    if not isinstance(model, keras.Sequential) and not isinstance(model, keras.models.Functional) and not isinstance(model, keras.models.Model):
        raise AssertionError('model should be one of: sequential, functional or Model')

    if not isinstance(batchSize, int):
        raise AssertionError('batchSize should be an integer')

    if not isinstance(volumeSize, tuple):
        raise AssertionError('volumeSize should be a tuple')

    if not isinstance(channelIn, int):
        raise AssertionError('channelIn should be an integer')

    if not isinstance(channelOut, int):
        raise AssertionError('channelOut should be an integer')

    if not isinstance(gpuDevice, list):
        raise AssertionError('gpuDevice should be a list')

    # required pre-defined dictionaries
    profilerDict = ['Selected GPUs', 'GPU memory required', 'Trainable model parameters']
    unitDict = ['GPU IDs', 'GB', 'Million']

    class Profiler:
        def __init__(self):
            self.devices = []
            self.memory = 0.0
            self.modelParams = 0

    # this should only be used hen the model uses the functional API - per layer
    functionalLayerIdx = 0
    functionalModelflag = False

    for layerItr, layer in enumerate(model.layers):
        if isinstance(layer, Functional):
            functionalLayerIdx = layerItr
            functionalModelflag = True

    varSize = 8 if model.layers[0].dtype == 'float32' else 16
    # varSizeToGb = (varSize * 1 / 10 ** 6) if model.layers[0].dtype == 'float32' else (varSize * 1 / 10 ** 9)
    varSizeToGb = (varSize / (1024 ** 3))
    dataSize = (batchSize * (volumeSize[0] * volumeSize[1] * volumeSize[-1])) * (channelIn + channelOut * 2) #  * varSizeToGb  # conversion to Gb
    dataSizeArray = np.zeros(shape=(dataSize), dtype=np.float32) if model.layers[0].dtype == 'float32' else np.zeros(shape=(dataSize), dtype=np.float64)
    dataSize = dataSizeArray.nbytes * varSizeToGb

    if not functionalModelflag:
        # gpuMem = model_profiler.utils.keras_model_memory_usage(unitDict[1], model, batchSize)

        # TODO update package utils using the custom one?
        # modelParam = model_profiler.utils.get_param(unitDict[2], model)
        nonTrainParams = count_params(model.non_trainable_weights)
        train_params = count_params(model.trainable_weights)
        modelParam = abs(nonTrainParams - train_params) / (10 ** 6) # 4 times this is practically required

        # TODO do it with a decorator + add a switch case for the datatype
        # rough estimation using float32 is 4bytes or float64 is 8bytes, datatype for 2D with a single channel for all trainable vars
        # in 64bit systems float overhead is 16 bytes but its value is 8 bytes
        gpuMem = (4 * modelParam * varSizeToGb) + dataSize

    if functionalModelflag:
        gpuMem = model_profiler.utils.keras_model_memory_usage(unitDict[1], model.layers[functionalLayerIdx], batchSize)
        modelParam = model_profiler.utils.get_param(unitDict[2], model.layers[functionalLayerIdx])

        gpuMem += model_profiler.utils.keras_model_memory_usage(unitDict[1], model.layers[functionalLayerIdx + 1:], batchSize)
        modelParam += model_profiler.utils.get_param(unitDict[2], model.layers[functionalLayerIdx + 1:])
        train_params = count_params(model.trainable_weights)
        modelParam = abs(modelParam - train_params) / (10 ** 6) # 4 times this is practically required

        # rough estimation using float32 datatype for all trainable vars
        # in 64bit systems float overhead is 16 bytes but its value is 8 bytes
        gpuMem = (4 * modelParam * varSizeToGb) + dataSize

    gpuMem = round(gpuMem, 2)
    modelParam = round(modelParam, 2)

    valuesDict = [gpuDevice, gpuMem, modelParam]

    # initializing profiler object
    p_info = Profiler()
    p_info.devices, p_info.memory, p_info.modelParams = gpuDevice, gpuMem, modelParam

    concProfileInfo = np.concatenate((np.expand_dims(np.array(profilerDict, dtype=object), axis=1),
                                      np.expand_dims(np.array(valuesDict, dtype=object), axis=1),
                                      np.expand_dims(np.array(unitDict, dtype=object), axis=1)), axis=1)

    profilerSummary = tabulate(list(concProfileInfo),
                               headers=["Model Profile", "Value", "Unit"],
                               tablefmt="github")

    print(profilerSummary)

    return p_info

def calculateMaxBatch(availableMemory: float, model:keras.models, volumeSize: tuple, channelIn: int, channelOut: int, gpuDevice: list):
    '''
    This function will calculate the required memory and information related to the given setup.
    NOTE: currently works only for 2D float32, single channel
    Parameters
    availableMemory: available total memory
    model : tensorflow model. Supports both sequential or functional
    volumeSize : tuple with the volume size
    channelIn : number of input channels
    channelOut : number of output channels

    Returns
    profile: object with memory requirements and information related to the current setup
    '''
    if not isinstance(availableMemory, int):
        raise AssertionError('availableMemory should be a float')

    if not isinstance(model, keras.Sequential) and not isinstance(model, keras.models.Functional) and not isinstance(model, keras.models.Model):
        raise AssertionError('model should be one of: sequential, functional or Model')

    if not isinstance(volumeSize, tuple):
        raise AssertionError('volumeSize should be a tuple')

    if not isinstance(channelIn, int):
        raise AssertionError('channelIn should be an integer')

    if not isinstance(channelOut, int):
        raise AssertionError('channelOut should be an integer')

    if not isinstance(gpuDevice, list):
        raise AssertionError('gpuDevice should be a list')

    # required pre-defined dictionaries
    unitDict = ['GPU IDs', 'GB', 'Million']

    # this should only be used hen the model uses the functional API - per layer
    functionalLayerIdx = 0
    functionalModelflag = False

    for layerItr, layer in enumerate(model.layers):
        if isinstance(layer, Functional):
            functionalLayerIdx = layerItr
            functionalModelflag = True

    varSize = 4 if model.layers[0].dtype == 'float32' else 8
    varSizeToGb = (varSize * 1 / 10 ** 6) if model.layers[0].dtype == 'float32' else (varSize * 1 / 10 ** 9)
    dataSize = (volumeSize[0] * volumeSize[1] * volumeSize[-1]) * (channelIn + channelOut * 2) * varSizeToGb  # conversion to Gb

    if not functionalModelflag:
        # gpuMem = model_profiler.utils.keras_model_memory_usage(unitDict[1], model, batchSize)

        # TODO update package utils using the custom one?
        # modelParam = model_profiler.utils.get_param(unitDict[2], model)
        nonTrainParams = count_params(model.non_trainable_weights)
        train_params = count_params(model.trainable_weights)
        modelParam = nonTrainParams - train_params

        # TODO do it with a decorator + add a switch case for the datatype
        # rough estimation using float32 is 4bytes or float64 is 8bytes, datatype for 2D with a single channel for all trainable vars
        # in 64bit systems float overhead is 16 bytes but its value is 8 bytes
        requiredModleMem = (train_params * varSizeToGb)

    if functionalModelflag:
        requiredMem = model_profiler.utils.keras_model_memory_usage(unitDict[1], model.layers[functionalLayerIdx], batchSize)
        modelParam = model_profiler.utils.get_param(unitDict[2], model.layers[functionalLayerIdx])

        requiredMem += model_profiler.utils.keras_model_memory_usage(unitDict[1], model.layers[functionalLayerIdx + 1:], batchSize)
        modelParam += model_profiler.utils.get_param(unitDict[2], model.layers[functionalLayerIdx + 1:])
        train_params = count_params(model.trainable_weights)
        modelParam = modelParam - train_params

        # rough estimation using float32 datatype for all trainable vars
        # in 64bit systems float overhead is 16 bytes but its value is 8 bytes
        requiredModleMem = (train_params * varSizeToGb)


    maxBatch = availableMemory - requiredModleMem // dataSize

    print('Max batch size for the current setup is:', maxBatch)

    return maxBatch

def createPhysicalDevices(availableMemory: list, requiredMemory: float, distributedLearning: bool):
    '''
    This function will partition the logical devices into physical (or virtual) devices to distribute your model training.
    Params:
    availableMemory:  list of available memory per devices
    requiredResources: float of required memory
    distributedLearning: boolean indicating if distributed learning enabled, further partitioning in the resources will be skipped.
    Returns: list with cuda devices and corresponding compatibility, list of available GPU
    '''

    if not (isinstance(availableMemory, list)):
        raise AssertionError('availableMemory is not a list.')

    if not (isinstance(requiredMemory, float)):
        raise AssertionError('requiredMemory is not a float.')

    if not (isinstance(distributedLearning, bool)):
        raise AssertionError('distributedLearning is not a bool.')

    # getting GPU info
    availableDevices = tf.config.list_physical_devices('GPU')

    maxSharedInstancesPerGpu = []

    if len(availableDevices) > 1:
        print('Multiple GPUs detected. Distributed learning is supported.')

    else:
        print('Single GPU detected. Single device will be used for distributed learning.')

    if len(availableDevices) >= 1 and not distributedLearning:
        try:
            for itrGpu, idxGpu in enumerate(availableDevices):
                # calculation is based on Gb - 1Gb is kept free for sharing tasks during prefetching/data offloading
                current_dev_memory = int(availableMemory[itrGpu] // requiredMemory) - 1
                maxSharedInstancesPerGpu.append(current_dev_memory)
                # physical devices cannot be initialized after being initialized
                # tf.config.experimental.set_memory_growth(idxGpu, True)
                list_config_temp = []

                for itrOptimalInstances in range(current_dev_memory):
                    list_config_temp.append(tf.config.LogicalDeviceConfiguration(memory_limit=requiredMemory))

                # TODO not sure for interaction with tf.distribution strategy and/or tf cpu handler
                tf.config.set_logical_device_configuration(device=availableDevices[itrGpu],
                                                           logical_devices=[list_config_temp])

            logical_gpus = tf.config.list_logical_devices('GPU')
            print(len(availableDevices), 'Physical GPUs', len(logical_gpus), 'Logical GPUs')

        except RuntimeError as e:
            print(e)

def optimizeConfig():
    '''
    This function will calculate if CUDA devices on the current setup can run using an optimized profiler based
    on the compatibility map.
    Params: none
    Returns: list with cuda devices and corresponding compatibility, list of available memory
    '''

    local_dev = device_lib.list_local_devices()

    gpuDev = [gpuItr for gpuItr in local_dev if gpuItr.device_type == 'GPU']

    gpuLst = []
    compCapabLst = []
    gpuMem = []

    for itrGpu in gpuDev:
        # TODO this should be refactored
        gpuMem.append(float(itrGpu.memory_limit * 1e-9))
        if 'compute capability' in itrGpu.physical_device_desc:
            device_desc_lst = list(itrGpu.physical_device_desc.split(", "))
            outLstTemp = [itrTemp_dev.replace(" ", "") for itrTemp_dev in device_desc_lst]
            outLstTemp2 = [itrTemp_dev.split(":") for itrTemp_dev in outLstTemp]
            # outLstTmep3 = [gpuLst.append(itrTemp_dev[-1]) for itrTemp_dev in outLstTemp2 if 'name' in itrTemp_dev]
            # outLstTemp4 = [compCapabLst.append(itrTemp_dev[-1]) for itrTemp_dev in outLstTemp2 if 'computecapability' in itrTemp_dev]

    # device summary
    gpuDeviceConfig = lstToDict(gpuLst, compCapabLst)

    # cuda capable devices
    print('-------------------------------')
    print('Device capable of optimized performance found are:', gpuDeviceConfig)
    print('-------------------------------')

    # mixed_precision available devices
    for itrDevice, itrBoost in enumerate(compCapabLst):
        if float(itrBoost) > 7.5:
            print('-------------------------------')
            print('Device', gpuLst[itrDevice], 'is capable to be used for optimized training.')
            print('-------------------------------')

    totalMem = sum(gpuMem)

    return gpuLst, gpuMem, totalMem

def modelSummary2Tab(model):
    """Quick function for create a tf.model summary into a beautiful table."""

    modelSummaryTab = pd.DataFrame(columns=["Name", "InShape", "OutShape", "TrainParams"])

    for itrLayer in model.layers:
        if isinstance(itrLayer, InputLayer):
            modelSummaryTab = modelSummaryTab.append({"Name": itrLayer.name,
                                                      # "Function": "InputLayer",
                                                      "InShape": itrLayer.input_shape,
                                                      "OutShape": itrLayer.output_shape,
                                                      "TrainParams": count_params(itrLayer.trainable_weights)},
                                                      ignore_index=True)
        else:
            modelSummaryTab = modelSummaryTab.append({"Name": itrLayer.name,
                                                  # "Function": itrLayer.__class__name__,
                                                  "InShape": itrLayer.output_shape,
                                                  "OutShape": itrLayer.output_shape,
                                                  "TrainParams": count_params(itrLayer.trainable_weights)},
                                                  ignore_index=True)

    return modelSummaryTab

if __name__ == "__main__":
    # exporting GPU to env vars
    gpuLength = dynamcGpuAlloc()
    # calculating optimization configurations
    currentDevices, perDeviceMemory, totalMemory = optimizeConfig()

    # default model
    model = keras.models.Sequential()
    model.add(EfficientNet(weights='imagenet', include_top=False, input_shape=(256, 256, 3)))
    batchSize = 4
    profile = modelProfiler(model=model, batchSize=batchSize, volumeSize=(192, 192, 192), channelIn=1, channelOut=1, gpuDevice=currentDevices)

    # optimization - it does not work with automatic partitioning
    # createPhysicalDevices(availableMemory=perDeviceMemory, requiredMemory=profile.memory, distributedLearning=False)


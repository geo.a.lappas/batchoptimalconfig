## Description
This is a script for calculating the optimal configuration for the batch size using Tensorflow/Keras models.
It also provides the user with a summary of the required model and GPU memory.


## Installation
Install first the the required libraries using pip in a virtual environment
- python 3.X
- tensorflow
- tabulate
- keras
- numpy
- pandas
- model_profiler

## Usage
1. Adjust the model and run configRun.py to calculate the required memory based on the available resources
2. Run createPhysicalDevices to partition the resources into physical devices for optimal parallelization

## Support
For issues, please create an issue or contact geo.a.lappas@gmail.com. 

## Contributing
Would you like to contribute? Please contact geo.a.lappas@gmail.com. 

## License
Apache 2.0 license

## Project status
Active.